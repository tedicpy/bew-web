This is a bare-bones example for using the [d3-layout-narrative](https://github.com/abcnews/d3-layout-narrative) module. It's a cut-down version of the [Episode IV chart](http://www.abc.net.au/news/2015-12-16/star-wars-every-scene/7013826#chapter1) in [Star Wars: every scene from I-VI charted](http://www.abc.net.au/news/2015-12-16/star-wars-every-scene/7013826) at [ABC News](http://www.abc.net.au/news/).

View the [annotated source](https://abcnews.github.io/d3-layout-narrative/) for more on the API.
